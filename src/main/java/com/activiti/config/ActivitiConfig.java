package com.activiti.config;

import com.alibaba.druid.pool.DruidDataSource;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.impl.cfg.StandaloneProcessEngineConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;


/**
 * @author liuc
 * @version V1.0
 * @date 2021/9/30 15:50
 * @since JDK1.8
 */
@Slf4j
@Configuration
public class ActivitiConfig {

    /**

     * 初始化配置，将创建25张表

     *

     * @return

     */

    @Bean

    public StandaloneProcessEngineConfiguration processEngineConfiguration() {

        StandaloneProcessEngineConfiguration configuration = new StandaloneProcessEngineConfiguration();

        configuration.setDataSource(dataSource());

        configuration.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);

        configuration.setAsyncExecutorActivate(false);

        return configuration;

    }

    /**

     * 创建引擎

     * @return ProcessEngine

     */

    @Bean
    public ProcessEngine processEngine() {

        return processEngineConfiguration().buildProcessEngine();

    }

    @Bean(name = "dataSource")
    @ConfigurationProperties(prefix = "spring.datasource" )
    public DruidDataSource dataSource () {
        return new DruidDataSource();
    }

}
